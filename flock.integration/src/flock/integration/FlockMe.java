package flock.integration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class FlockMe {
	
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		sb.append("sahil");
		sb.append("\\n");	
		sb.append("arora1");		
		sendTestCaseStatus(sb.toString());
		
	}

	public static void sendTestCaseStatus(String message) {
		String line;
		StringBuffer jsonString = new StringBuffer();
		try {

			URL url = new URL("https://api.flock.co/hooks/sendMessage/b6ea7f72-72b5-41ad-ac59-1d43ec6977ee");
			
			String payload = "{\"text\": \"" + message +  "\" }";

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
			writer.write(payload);
			writer.close();
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = br.readLine()) != null) {
				jsonString.append(line);
			}
			br.close();
			connection.disconnect();
			System.out.println("Flock message sent successfully");
			
		} catch (Exception e) {
			System.out.println("Flock message sending failed");
			System.out.println(e.getMessage());			
		}		
	}
	
}
