package test.json.response;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.*;

import com.sun.xml.internal.bind.v2.runtime.RuntimeUtil.ToStringAdapter;

public class NetClientGet {

	public static void main(String[] args)  {

		/*String firstURL = "http://feedsim.mnetads.com", 
				secondURL = "http://feed.mnetads.com", 
				path = "",
				pathCollectionFile = args[0],
				jsonMatchResult = args[1];
		URL url;
		boolean jsonMatch;
		JSONObject json1, json2;
		int pathNumber = 0;
		BufferedReader br = null; */
		JSONObject json;
		 
		try {
			/*System.out.println("Started"); 
			br = new BufferedReader(new FileReader(pathCollectionFile));
			File file = new File(jsonMatchResult);
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw); 
			while ((path = br.readLine()) != null) {
				url = new URL(firstURL.concat(path));
				json1 = getJson(url);
				url = new URL(secondURL.concat(path));
				json2 = getJson(url);
				jsonMatch = compareJSON(json1,json2);
				if(!jsonMatch){
					bw.newLine();
					bw.write("Failed for Path: " + path);
					bw.newLine();
					bw.write("JSON1: " + json1);
					bw.newLine();
					bw.write("JSON2: " + json2);
					bw.newLine();
				}
                bw.flush();
				System.out.println("Tested Path No. = " + pathNumber + "; Result: " + jsonMatch);
				pathNumber++;
			}
			bw.close();
			*/
			URL url = new URL("http://feedtest.mnetads.com/v2/ads?partid=285380A4-34B6-4729-B821-2EE2870EFAE1&pid=2.1&purl=https://www.media.net&numofads=10&uip=68.71.131.9&uua=Mozilla/5.0%20(Windows%20NT%206.1;%20WOW64;%20rv:46.0)%20Gecko/20100101%20Firefox/46.0&ch=Test-Channel&term=shoes&spid=2.2&sspid=2.3&c=Footwear&mkt=tstdefault&rurl=http://www.google.com");
			json = getJson(url);
			System.out.println("numberOfAds: " + json.getJSONObject("header").getString("numberOfAds")); 
			for (int adIndex = 0; adIndex < json.getJSONArray("ads").length(); adIndex++) {
				System.out.println(json.getJSONArray("ads").getJSONObject(adIndex).getString("adId")); 
			} 
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
	}
	
	public static boolean compareJSON(JSONObject json1, JSONObject json2){
		boolean numberOfAds=true,adKey=true,creativeKeyImages=true,creativeKeyText=true,bid=true,jsonMatch=true;
		try {
			if (json1.getJSONObject("header").getString("numberOfAds").equals(json2.getJSONObject("header").getString("numberOfAds"))) {
				numberOfAds = true;

				for (int adIndex = 0; adIndex < json1.getJSONArray("ads").length(); adIndex++) {
					
					if (json1.getJSONArray("ads").getJSONObject(adIndex).getString("adKey").equals(json1.getJSONArray("ads").getJSONObject(0).getString("adKey"))) {
						adKey = true;
					} else {
						adKey = false;
						break;
					}

					if (json1.getJSONArray("ads").getJSONObject(adIndex).getString("bid")
							.equals(json1.getJSONArray("ads").getJSONObject(0).getString("bid"))) {
						bid = true;
					} else {
						bid = false;
						break;
					}

					if (json1.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("images").length() > 0
							&& json2.getJSONArray("ads").getJSONObject(0).getJSONObject("creatives").getJSONArray("images").length() > 0
							&& json1.getJSONArray("ads").getJSONObject(0).getJSONObject("creatives").getJSONArray("images").length() == 
								json2.getJSONArray("ads").getJSONObject(0).getJSONObject("creatives").getJSONArray("images").length()) {
						for (int item = 0; item < json1.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("images").length(); item++) {
							if (json1.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("images").getJSONObject(item).getString("creativeKey") != 
								json2.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("images").getJSONObject(item).getString("creativeKey")) {
								creativeKeyImages = true;
							} else {
								creativeKeyImages = false;
								break;
							}
						}
					}
					
					if (json1.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("texts").length() > 0
							&& json2.getJSONArray("ads").getJSONObject(0).getJSONObject("creatives").getJSONArray("texts").length() > 0
							&& json1.getJSONArray("ads").getJSONObject(0).getJSONObject("creatives").getJSONArray("texts").length() == 
								json2.getJSONArray("ads").getJSONObject(0).getJSONObject("creatives").getJSONArray("texts").length()) {
						for (int item = 0; item < json1.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("texts").length(); item++) {
							if (json1.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("texts").getJSONObject(item).getString("creativeKey") != 
								json2.getJSONArray("ads").getJSONObject(adIndex).getJSONObject("creatives").getJSONArray("texts").getJSONObject(item).getString("creativeKey")) {
								creativeKeyText = true;
							} else {
								creativeKeyText = false;
								break;
							}
						}
					}
					
				}

			} else {
				numberOfAds = false;
			} 
		} catch (Exception e) {
			System.out.println("JSON Exception caused while matching 2 JSON");
		}	
//		System.out.println("adKey= " + adKey +  ",bid=" + bid +  ", numberOfAds=" + numberOfAds +  ",creativeKeyImages=" + creativeKeyImages +  ",creativeKeyText=" + creativeKeyText);
		if(adKey==false || bid == false  || numberOfAds == false || creativeKeyImages == false || creativeKeyText == false){
			jsonMatch=false;
			System.out.println("adKey= " + adKey +  ",bid=" + bid +  ", numberOfAds=" + numberOfAds +  ",creativeKeyImages=" + creativeKeyImages +  ",creativeKeyText=" + creativeKeyText);
		} else {
			jsonMatch=true;
		}
		return jsonMatch;		
	}
	 
	public static JSONObject getJson(URL url) {
		JSONObject json = new JSONObject(); 
	  try {

		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		StringBuilder sb = new StringBuilder();
		
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		try {
		json = new JSONObject(sb.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.disconnect();

	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	  }
	return json;

	}

}